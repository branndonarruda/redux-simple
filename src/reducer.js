const initialState = {
    text: 'Texto inicial Store',
    counter: 0,
    items: ['iPhone', 'iPad', 'iPod']
};

// shared variables
let counter = initialState.counter;

/**
 * function to decrement counter
 */
function decrement() {
    if (counter >= 1) {
        return (counter - 1);
    } else {
        return counter;
    }
}

/**
 * function to increment counter
 */
function increment() {
    return (counter+1);
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SET_TEXT':
            return { ...state, text: action.payload }
        case 'INCREMENT':
                return {
                    ...state,
                    counter: increment()
                }
        case 'DECREMENT':
                return {
                    ...state,
                    counter: decrement()
                }
        case 'ADD_TO_LIST':
            return {
                ...state,
                // items: state.items.concat(action.payload)
                items: [...state.items, action.payload]
            }
        default:
            return state
    }
}