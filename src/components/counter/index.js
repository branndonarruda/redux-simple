import
    React,
    {Component} from 'react';

// scss
import './counter.scss';

class Counter extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    /** send action to container
     * and execute action
     */
    handleClick = (action) => {
        this.props.onButtonClick(action);
    }

    render(){
        return(
            <div className="counter">
                <button
                    className="counter__button"
                    onClick={
                        (event) => {
                            this.handleClick('increment');
                        }
                    }
                >
                    +
                </button>

                <span className="counter__label">{this.props.number}</span>

                <button
                    className="counter__button"
                    onClick={
                        () => {
                            this.handleClick('decrement')
                        }
                    }
                >
                    -
                </button>
            </div>
        );
    }
}

export default Counter;