import React, {Component} from 'react';

class Input extends Component {
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e){
        this.props.changeValue(e);
    }

    render(){
        return(
            <input
                value={this.props.value}
                name={this.props.name}
                placeholder={this.props.placeholder}
                onChange={this.handleClick}
                />
        );
    }
}

export default Input;