export function changeText(text) {
    return {
      type: 'SET_TEXT',
      payload: text
    }
}

export function increment(value) {
    return {
        type: 'INCREMENT',
        payload: value
    }
}

export function decrement(value) {
    return {
        type: 'DECREMENT',
        payload: value
    }
}

export function addToList(value) {
    return {
        type: 'ADD_TO_LIST',
        payload: value
    }
}