import React, { Component } from 'react';
import './App.css';
// import store from './store';

// react-redux
import { connect } from 'react-redux';
import { changeText, increment, decrement, addToList } from './actions';

// components
import Counter from './components/counter';
import Input from './components/input/input';
import { AST_Debugger } from '../node_modules/terser';

class App extends Component {

  constructor(props) {
    super(props);
    this.getInputValue = this.getInputValue.bind(this);
    this.listItems = this.listItems.bind(this);
    this.addToList = this.addToList.bind(this);
  }

  /** change counter number 
   * counter++ or counter--
   * @param {string} action 
   */
  changeCounterValue = (action) => {
    if (action === 'decrement') {
      this.props.decrement();
    } else {
      this.props.increment();
    }
  }

  getInputValue = (event) => {
    console.log(event.value);
  }

  listItems() {
    const items = this.props.items;
    return items.map((item, index) =>
      <li key={index}>{item}</li>
    );
  }

  addToList () {
    this.props.addToList('joao');
  }

  render() {
    const results = this.listItems();

    return (
      <div className="App">
        <header className="App__header">
          <h1>React - Redux</h1>
        </header>
  
        <div className="App__body">

          {/* counter component */}
          <Counter number={this.props.counter} onButtonClick={this.changeCounterValue} />

          <span>{this.props.text}</span>

          <button
            onClick={() => {
              this.props.onTodoClick('oba oba')
            }
            }>Teste</button>

            
            <div className="App__body__input">
              <Input value="" name="" placeholder="Digite um item" changeValue={this.getInputValue} />
              <button onClick={this.addToList}>Adicionar</button>
            </div>
            
            <ul>
              {results}
            </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    text: state.text,
    counter: state.counter,
    items: state.items
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onTodoClick: (text) => {
      dispatch(changeText(text))
    },

    increment: () => {
      dispatch(increment());
    },

    decrement: () => {
      dispatch(decrement());
    },

    addToList: (text) => {
      dispatch(addToList(text));
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);